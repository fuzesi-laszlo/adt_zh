﻿# #EzNemPuska

## Adatbáziskészítés

Függőségek megadása, például a projektfájlban `(.csproj)` így:

```csharp
<ItemGroup>
	<PackageReference Include="Microsoft.EntityFrameworkCore" Version="5.0.12" />
	<PackageReference Include="Microsoft.EntityFrameworkCore.Proxies" Version="5.0.12" />
	<PackageReference Include="Microsoft.EntityFrameworkCore.SqlServer" Version="5.0.12" />
</ItemGroup>
```

Entitás-osztályok elkészítése és attribútumok elhelyezése

```csharp
[Table("PET")]
public class Pet
{
	[Key]
	[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
	public int Id { get; set; }

	[MaxLength(32)]
	[Required]
	public string Name { get; set; }

	[Column("Pet_habitat", TypeName = "string")]
	public string Habitat { get; set; }

	[ForeignKey(nameof(Owner))]
	public int OwnerId { get; set; }

	[NotMapped]
	public virtual Owner Owner { get; } // csak getter kell!
}

[Table("OWNER")]
public class Owner
{
	public Owner()
	{
		this.Pets = new HashSet<Pet>(); // enélkül nincs használható kapcsolat a 2 tábla között
	}

	[Key]
	[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
	public int Id { get; set; }

	[MaxLength(64)]
	[Required]
	public string Name { get; set; }

	[Column("Owner_address", TypeName = "string")]
	public string Address { get; set; }

	[NotMapped]
	public virtual ICollection<Pet> Pets { get; } // init a ctor-ban!
}
```

Saját kontextus megírása (táblák, kapcsolódási metódus, modellezés-konfiguráció)

```csharp
public partial class MyDbContext : DbContext
{
	public virtual DbSet<Owner> Owners { get; set; } // virtuális, de van getter és setter is
	public virtual DbSet<Pet> Pets { get; set; }

	public MyDbContext() => this.Database.EnsureCreated(); // ctor csak ennyi, egy aranyos lambda

protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
{
	if (!optionsBuilder.IsConfigured) // ha NEM konfigurált még, akkor:
	{
		optionsBuilder
			.UseLazyLoadingProxies() // el ne maradjon
			.UseSqlServer(@"Data Source=(LocalDB)\MSSQLLocalDB; AttachDbFilename=|DataDirectory|\EisaDb.mdf; Integrated Security=True; MultipleActiveResultSets=True;");	// ezt egye meg a rák
	}
}

protected override void OnModelCreating(ModelBuilder modelBuilder)
{
	modelBuilder.Entity<Pet>(entity =>
	{
		entity.HasOne(pet => pet.Owner)
			.WithMany(brand => brand.Models)
			.HasForeignKey(model => model.BrandId)
			.OnDelete(DeleteBehavior.Cascade); // vagy ClientSetNull, ha az Adatréteg alatt lévő SqlServer-re bízzuk
	});

	Pet pet1, pet2; // new Pet() {...} vagy ahonnan éppen adatot kapunk
	modelbuilder.Entity<Pet>.HasData(pet1, pet2);
}
```

## Reflexió + Attribútumok
Saját attribútumot lehet a VS-sel generáltatni, de kézzel is megoldható:

```csharp
[System.AttributeUsage(System.AttributeTargets.Property, AllowMultiple = false)]
public sealed class StringableAttribute : System.Attribute
{}
```

Főként egyéni attribútumok használatának felderítésére használunk mi reflexiót.
Típus megállapítása (osztályból vagy példányból), aztán az attribútummal nem rendelkező tagok kiszűrése.
Keressük a saját attribútumunkat és amin ez rajta van, kérjük ki a benne tárolt adatot:

```csharp
public static class XtensionClass
{
	public static string StringableToString<T>(this T stringableTypeObject)
	{
		var tType = typeof(T); // = stringableTypeObject.GetType()
		var sb = new System.Text.StringBuilder($"{tType.Name} ( ");
		var stringableProperties = tType.GetProperties()
			.Where(propertyInfo => propertyInfo.GetCustomAttributes(typeof(StringableAttribute), false).Length > 0);

		// ha üres a megszűrt lista, ilyet is return-olhatnnák:
		// if (!stringableProperties.Any())
		//	return $"No {nameof(StringableAttribute)} properties in {tType.Name}!";

		foreach (var stringableProp in stringableProperties)
		{
			sb.Append(stringableProp.Name + " = " + stringableProp.GetValue(stringableTypeObject) + ", ");
		}

		return sb.Append(')').ToString();
	}
}
```

Használata egy kézzel megírt `ToString()` helyett:

```csharp
public override string ToString() => this.StringableToString(); // igen, ennyi.
```

## LINQ + XML

Serial-izáljunk be egy táblát egyetlen XML fájlba:

```csharp
public static string ConvertObjectsToXml<T>(this IEnumerable<T> source)
{
	if (source is null)
		throw new System.ArgumentNullException(nameof(source));

	string result = string.Empty;
	var type = typeof(T);
	var myroot = new System.Xml.Linq.XElement(type.Name.ToUpperInvariant() + "_TABLE");
	var nonvirtualProperties = type
		.GetProperties()
		.Where(propertyInfo => !propertyInfo.GetGetMethod().IsVirtual)
		.ToArray();
	foreach (T item in source)
	{
		// Every property should be an XML attribute? Here you go:
		var attributes = new List<XAttribute>(capacity: nonvirtualProperties.Length);

			foreach (var propertyInfo in nonvirtualProperties)
			{
				attributes.Add(new XAttribute(propertyInfo.Name,propertyInfo.GetValue(item)));
				// ebből lesz az, hogy: 	Id = "1" Name = "buk$$Hi, gyereidetakarodj"
			}
			myroot.Add(new XElement(type.Name, attributes)); // itt már teljes a node: <Pet Id = "1" Name = "buk$$Hi, gyereidetakarodj" />

//			----- OR -----

			// Each item is a node. The first property is an XML attribute of that node.
			var node = new System.Xml.Linq.XElement(
				type.Name,
				new System.Xml.Linq.XAttribute(nonvirtualProperties[0].Name, nonvirtualProperties[0].GetValue(item)));
			// All the rest (starting with the second property, index 1) should be a child node => the child node's name is the prop. name, its content is the prop's value.
			for (int i = 1; i < nonvirtualProperties.Length; i++)
			{
				node.Add(new System.Xml.Linq.XElement(
					nonvirtualProperties[i].Name,
					nonvirtualProperties[i].GetValue(item)));
			}
			myroot.Add(node); // emebből pedig az lesz, hogy: 	<Pet Id="1">
		//															<Name>buk$$Hi, gyereidetakarodj</Name>
		//														</Pet>
	}
	var xDocument = new XDocument(
		new XDeclaration("1.0", "utf-8", "yes"),
		myroot);

	using (System.IO.StringWriter sw = new System.IO.StringWriter())
	{
		xDocument.Save(sw); 	// errefelé lehetne fájlba menteni
		result = sw.ToString(); // de én string-et kérek
	}
	return result;
}
```

És visszafelé? Adott az XML, de nem tudjuk milyen típust tárol és milyen adatot milyen 'altípusként' tart magában (szám? betű? dátum?)
Mi most csak egész számokkal és szöveggel dolgozunk:

```csharp
public static IEnumerable<object> ConvertXmlToObjects(this XDocument xDocument)
{
	if (xDocument is null)
		throw new System.ArgumentNullException(nameof(xDocument));

	string tableName = xDocument.Root.Name.LocalName.Split('_')[0]; // ez fekete mágia, nálam a root ilyen: <TABLE_NAME> ezért kellet szétvágni és az első részét venni alapul. Pl: "BRAND_TABLE" --> "BRAND" --> Brand típus (konverzió lentebb)
	var objects = new List<object>();
	var typeDict =
		System.Reflection.Assembly.Load("EisaAwards.Data").GetTypes() // kérem az összes osztályt adott névtérből
		.Where(type => !type.FullName.Contains("<>")) // a teljes neve (pl EisaAwards.Data.Brand) nem tartalmazhat kacsacsőröket (mert azok a lusta osztályok, amiket az EFCore LazyLoading generál futásidőben, nem kellenek)
		.ToDictionary(t => t.Name.ToUpperInvariant());  // dobjuk be a típusokat reprezentáló Type objektumokat és a típusok (string) neveit egy dictionary-be
	var type = typeDict[tableName]; // kérem azt a típust, ami megfelel az XML gyökerében lévő szóval
	var nonvirtualProperties = type
		.GetProperties()
		.Where(propertyInfo => !propertyInfo.GetGetMethod().IsVirtual) // a virtual property-ket itt is mellőzzük
		.ToArray();
    
	foreach (var element in xDocument.Root.Descendants(type.Name)) // végigmegyek az XML összes node-ján, hisz' mindegyik a gyökéren belül van és ugyanúgy <MyType> <Id>1</Id> <Name>Neve</Name> </MyType> alakúak
    {
        var instance = System.Activator.CreateInstance(type);
        nonvirtualProperties[0].SetValue(
            instance,
            int.Parse(element.Attribute(nonvirtualProperties[0].Name).Value, System.Globalization.NumberFormatInfo.InvariantInfo));
        var subElements = element.Descendants().ToArray();

		for (int i = 0; i < subElements.Length; i++)
        {
            var propInfo = nonvirtualProperties[i + 1];
            if (propInfo.PropertyType == typeof(string))
            {
                propInfo.SetValue(
                    instance,
                    subElements[i].Value);
            }
            else if (propInfo.PropertyType == typeof(int))
            {
                propInfo.SetValue(
                    instance,
                    int.Parse(subElements[i].Value, System.Globalization.NumberFormatInfo.InvariantInfo));
            }
        }
        objects.Add(instance);
    }
    return objects;
}
```

## Egységtesztelés

VS-ben generáltatunk egy NUnit teszt projektet, függőségeket magától beadja, vázlatos osztályt is ír.
Legyen `[TestFixture]` és a többi.
`[TestCase()]` -ben csak konstans (értékek) lehet(nek).
